
package com.sololearn.theguardiannews.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("total")
    @Expose
    public int total;
    @SerializedName("startIndex")
    @Expose
    public int startIndex;
    @SerializedName("pageSize")
    @Expose
    public int pageSize;
    @SerializedName("currentPage")
    @Expose
    public int currentPage;
    @SerializedName("pages")
    @Expose
    public int pages;
    @SerializedName("orderBy")
    @Expose
    public String orderBy;
    @SerializedName("results")
    @Expose
    public List<Article> articles = null;

    public String getStatus() {
        return status;
    }

    public int getTotal() {
        return total;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getPages() {
        return pages;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public List<Article> getArticles() {
        return articles;
    }
}
