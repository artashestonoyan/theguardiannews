
package com.sololearn.theguardiannews.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Fields extends RealmObject implements Parcelable {

    @SerializedName("thumbnail")
    @Expose
    public String thumbnail;

    @SerializedName("body")
    @Expose
    public String body;

    public String getThumbnail() {
        return thumbnail;
    }

    public String getBody() {
        return body;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.thumbnail);
        dest.writeString(this.body);
    }

    public Fields() {
    }

    protected Fields(Parcel in) {
        this.thumbnail = in.readString();
        this.body = in.readString();
    }

    public static final Parcelable.Creator<Fields> CREATOR = new Parcelable.Creator<Fields>() {
        @Override
        public Fields createFromParcel(Parcel source) {
            return new Fields(source);
        }

        @Override
        public Fields[] newArray(int size) {
            return new Fields[size];
        }
    };
}
