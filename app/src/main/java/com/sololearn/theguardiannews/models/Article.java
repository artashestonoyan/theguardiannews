
package com.sololearn.theguardiannews.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Article extends RealmObject implements Parcelable {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("webPublicationDate")
    @Expose
    private String webPublicationDate;
    @SerializedName("webTitle")
    @Expose
    private String webTitle;
    @SerializedName("apiUrl")
    @Expose
    private String apiUrl;
    @SerializedName("fields")
    @Expose
    private Fields fields;
    @SerializedName("pillarId")
    @Expose
    private String pillarId;
    @SerializedName("pillarName")
    @Expose
    private String pillarName;

    private boolean isFavorite;

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getWebPublicationDate() {
        return webPublicationDate;
    }

    public String getWebTitle() {
        return webTitle;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public Fields getFields() {
        return fields;
    }

    public String getPillarId() {
        return pillarId;
    }

    public String getPillarName() {
        return pillarName;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.type);
        dest.writeString(this.webPublicationDate);
        dest.writeString(this.webTitle);
        dest.writeString(this.apiUrl);
        dest.writeParcelable(this.fields, flags);
        dest.writeString(this.pillarId);
        dest.writeString(this.pillarName);
    }

    public Article() {
    }

    protected Article(Parcel in) {
        this.id = in.readString();
        this.type = in.readString();
        this.webPublicationDate = in.readString();
        this.webTitle = in.readString();
        this.apiUrl = in.readString();
        this.fields = in.readParcelable(Fields.class.getClassLoader());
        this.pillarId = in.readString();
        this.pillarName = in.readString();
    }

    public static final Parcelable.Creator<Article> CREATOR = new Parcelable.Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel source) {
            return new Article(source);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };
}
