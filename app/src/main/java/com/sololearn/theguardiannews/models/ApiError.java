package com.sololearn.theguardiannews.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApiError {

    @SerializedName("response")
    @Expose
    public Error error;

    public Error getError() {
        return error;
    }
}
