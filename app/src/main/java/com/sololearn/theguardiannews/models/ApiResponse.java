
package com.sololearn.theguardiannews.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApiResponse {

    @SerializedName("response")
    @Expose
    public Response response;

    public Response getResponse() {
        return response;
    }
}
