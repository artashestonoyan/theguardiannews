package com.sololearn.theguardiannews;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sololearn.theguardiannews.models.Article;

import java.util.List;

import androidx.core.app.ActivityOptionsCompat;
import androidx.recyclerview.widget.RecyclerView;

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.ViewHolder> {

    private boolean mIsVertical;
    private List<Article> mArticles;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView category;
        public ImageView thumbnail;

        public ViewHolder(View v) {
            super(v);
            title = v.findViewById(R.id.article_title);
            category = v.findViewById(R.id.article_category);
            thumbnail = v.findViewById(R.id.article_thumbnail);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(mContext, ArticleDetailsActivity.class);
                    intent.putExtra(ArticleDetailsActivity.EXTRA_ARTICLE, mArticles.get(getAdapterPosition()));
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation((Activity) mContext, thumbnail, "thumbnail");
                    mContext.startActivity(intent, options.toBundle());
                }
            });
        }
    }

    public NewsListAdapter(Context context, List<Article> articles, boolean isVertical) {
        mContext = context;
        mArticles = articles;
        mIsVertical = isVertical;
    }

    @Override
    public NewsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {

        View v = null;
        if (mIsVertical) {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_news_list, parent, false);
        }
        else {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_favorites_list, parent, false);
        }

        NewsListAdapter.ViewHolder vh = new NewsListAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(NewsListAdapter.ViewHolder holder, int position) {

        Article article = mArticles.get(position);
        if (article == null) {
            return;
        }

        holder.title.setText(article.getWebTitle());
        holder.category.setText(article.getPillarName());

        if (article.getFields() != null && article.getFields().getThumbnail() != null) {
            GlideApp.with(mContext)
                    .load(article.getFields().getThumbnail())
                    .placeholder(R.drawable.placeholder)
                    .into(holder.thumbnail);
        }
    }

    public void setArticles(List<Article> articles) {
        mArticles = articles;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mArticles != null ? mArticles.size() : 0;
    }
}