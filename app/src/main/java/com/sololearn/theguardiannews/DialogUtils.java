package com.sololearn.theguardiannews;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

public class DialogUtils {

    public static ProgressDialog showProgressDialog(Activity activity, String message, boolean cancelable) {

//        ProgressBar progressBar = new ProgressBar(activity);
//        progressBar.setIndeterminate(true);

        ProgressDialog progress = ProgressDialog.show(activity, null, message, true, cancelable);
        progress.setCanceledOnTouchOutside(false);

        return progress;
    }



    public static void showNoInternetAlertDialog(Activity context) {
        showSimpleAlertDialog(context, "Alert", context.getString(R.string.no_internet_message));
    }

    public static void showSimpleAlertDialog(Activity context, String message) {
        showSimpleAlertDialog(context, "Alert", message);
    }

    public static void showSimpleAlertDialog(Activity context, String title, String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setCancelable(true)
                .show();
    }
}
