package com.sololearn.theguardiannews.networking;

import com.sololearn.theguardiannews.models.ApiResponse;

public interface ResultListener<T> {
    void onRequestSuccess(ApiResponse response) throws Throwable;
    void onRequestFailed(String errorMessage);
}
