package com.sololearn.theguardiannews.networking;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sololearn.theguardiannews.BuildConfig;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {

    public static final String BASE_URL = "https://content.guardianapis.com/";

    private static Api mInstance = new Api();

    private static ApiInterface mAPIInterface;

    public static ApiInterface getApiInterface() {
        if (mAPIInterface == null) {
            mInstance = new Api();
        }
        return mAPIInterface;
    }

    private Api() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .client(getHttpClient())
                .build();

        mAPIInterface = retrofit.create(ApiInterface.class);
    }

    public Gson getGson() {
        return new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.STATIC)
                .excludeFieldsWithoutExposeAnnotation()
                .create();
    }

    private OkHttpClient getHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(loggingInterceptor);
        }

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {

                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()
//                        .header("Content-Type", "application/json")
                        .header("api-key", "test");

                requestBuilder.method(original.method(), original.body());

                Request request = requestBuilder.build();

                return chain.proceed(request);
            }
        });

        builder.readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS);

        return builder.build();
    }

    public static Api getInstance() {
        return mInstance;
    }
}
