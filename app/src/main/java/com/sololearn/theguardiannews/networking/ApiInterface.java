package com.sololearn.theguardiannews.networking;

import com.sololearn.theguardiannews.models.ApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("search?show-fields=thumbnail,body&order-by=newest&format=json")
    Call<ApiResponse> getNews(@Query("page") int page);
}
