package com.sololearn.theguardiannews.networking;

import android.app.Activity;
import android.content.Context;

import com.google.gson.JsonElement;
import com.sololearn.theguardiannews.DialogUtils;
import com.sololearn.theguardiannews.R;
import com.sololearn.theguardiannews.TheGuardianNewsApplication;
import com.sololearn.theguardiannews.models.ApiError;
import com.sololearn.theguardiannews.models.ApiResponse;
import com.sololearn.theguardiannews.models.Error;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiClient {

    private Context mContext;

    public ApiClient(Context mContext) {
        this.mContext = mContext;
    }

    public void makeRequestAsync(Call request, final ResultListener rl) {
        if (!TheGuardianNewsApplication.getInstance().isOnline()) {
//            rl.onRequestFailed(mContext.getString(R.string.no_internet_message));
            return;
        }

        request.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.isSuccessful()) {
                    try {
                        rl.onRequestSuccess((ApiResponse) response.body());
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }
                else {
                    if (response.errorBody() != null) {

                        try {
                            JSONObject json = new JSONObject(response.errorBody().string());

                            ApiError apiError = Api.getInstance().getGson().fromJson(json.toString(), ApiError.class);
                            if (apiError != null) {
                                showErrorDialog(apiError.getError().getMessage());
                                rl.onRequestFailed(apiError.getError().getMessage());
                            }
                            else {
                                showErrorDialog(mContext.getString(R.string.something_went_wrong_message));
                            }
                        } catch (JSONException | IOException e) {
                            showErrorDialog(mContext.getString(R.string.something_went_wrong_message));
                            e.printStackTrace();
                        }
                    }
                    JsonElement json = Api.getInstance().getGson().toJsonTree(response.body());

                    Error error = Api.getInstance().getGson().fromJson(json, Error.class);
                    if (error != null) {
                        showErrorDialog(error.getMessage());
                        rl.onRequestFailed(error.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                if (t instanceof IOException) {
                    if (mContext instanceof Activity) {
                        DialogUtils.showNoInternetAlertDialog((Activity) mContext);
                    }
                    rl.onRequestFailed(mContext.getString(R.string.no_internet_message));
                }
                else {
                    showErrorDialog(mContext.getString(R.string.something_went_wrong_message));
                    rl.onRequestFailed(mContext.getString(R.string.something_went_wrong_message));
                }

            }
        });
    }

    private String parseErrorBody(Response response) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        reader = new BufferedReader(new InputStreamReader(response.errorBody().byteStream()));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    private void showErrorDialog(String message) {
        if (mContext instanceof Activity) {
            DialogUtils.showSimpleAlertDialog((Activity) mContext, message);
        }
    }
}
