package com.sololearn.theguardiannews;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.sololearn.theguardiannews.models.Article;

import androidx.appcompat.app.AppCompatActivity;
import io.realm.Realm;

public class ArticleDetailsActivity extends AppCompatActivity {

    public static final String EXTRA_ARTICLE = "article";

    private Article mArticle;

    private ImageView mFavoriteIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_details);

        supportPostponeEnterTransition();

        mArticle = getIntent().getParcelableExtra(EXTRA_ARTICLE);

        mFavoriteIcon = findViewById(R.id.article_fav_icon);

        if (mArticle != null) {
            mFavoriteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mArticle.isFavorite()) {
                        removeArticleFromFavorites();
                    }
                    else {
                        saveArticleToFavorites();
                    }
                }
            });
            checkFavState();

            TextView title = findViewById(R.id.article_title);
            TextView category = findViewById(R.id.article_category);
            TextView content = findViewById(R.id.article_content);
            ImageView thumbnail = findViewById(R.id.article_thumbnail);

            title.setText(mArticle.getWebTitle());
            category.setText(mArticle.getPillarName());

            if (mArticle.getFields() != null) {

                content.setText(Html.fromHtml(mArticle.getFields().getBody()));

                GlideApp.with(this)
                        .load(mArticle.getFields().getThumbnail())
                        .placeholder(R.drawable.placeholder)
//                        .dontAnimate()
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                supportStartPostponedEnterTransition();
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                supportStartPostponedEnterTransition();
                                return false;
                            }
                        })
                        .into(thumbnail);
            }
        }
    }

    private void checkFavState() {
        try (Realm realm = Realm.getDefaultInstance()) {

            Article article = realm.where(Article.class)
                    .equalTo("id", mArticle.getId())
                    .findFirst();
            if (article != null) {
                if (article.isFavorite()) {
                    mArticle.setFavorite(true);
                    mFavoriteIcon.setImageResource(R.drawable.ic_favorite);
                }
                else {
                    mArticle.setFavorite(false);
                    mFavoriteIcon.setImageResource(R.drawable.ic_favorite_border);
                }
            }
        }
    }

    private void saveArticleToFavorites() {
        try (Realm realm = Realm.getDefaultInstance()) {

            realm.executeTransactionAsync(new Realm.Transaction() {
                  @Override
                  public void execute(Realm realm) {
                      mArticle.setFavorite(true);
                      realm.copyToRealmOrUpdate(mArticle);
                  }
              },
                new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        mFavoriteIcon.setImageResource(R.drawable.ic_favorite);
                    }
                },
                new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        mArticle.setFavorite(false);
                    }
                });
        }
    }

    private void removeArticleFromFavorites() {
        try (Realm realm = Realm.getDefaultInstance()) {

            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    mArticle.setFavorite(false);
                    realm.copyToRealmOrUpdate(mArticle);
                }
            },
            new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    mFavoriteIcon.setImageResource(R.drawable.ic_favorite_border);
                }
            },
            new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    mArticle.setFavorite(true);
                }
            });
        }
    }

    @Override
    public void onBackPressed() {

        supportFinishAfterTransition();
    }
}
