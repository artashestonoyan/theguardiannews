package com.sololearn.theguardiannews;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class TheGuardianNewsApplication extends Application {

    private static TheGuardianNewsApplication mInstance;

    public TheGuardianNewsApplication() {
        super();
    }

    public static TheGuardianNewsApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();


        mInstance = this;

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().name("theguardiannews.realm").deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(config);
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
