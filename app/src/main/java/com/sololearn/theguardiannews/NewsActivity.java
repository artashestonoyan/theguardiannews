package com.sololearn.theguardiannews;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.sololearn.theguardiannews.models.ApiResponse;
import com.sololearn.theguardiannews.models.Article;
import com.sololearn.theguardiannews.models.Response;
import com.sololearn.theguardiannews.networking.Api;
import com.sololearn.theguardiannews.networking.ApiClient;
import com.sololearn.theguardiannews.networking.ResultListener;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class NewsActivity extends AppCompatActivity {

    private Response mNewsResponse;
    private ApiClient mApiClient;
    private Realm mRealm;

    private RecyclerView mRecycler;
    private RecyclerView mFavoritesRecycler;
    private NewsListAdapter mAdapter;
    private EndlessRecyclerViewScrollListener scrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        mApiClient = new ApiClient(this);
        mRealm = Realm.getDefaultInstance();

        mRecycler = findViewById(R.id.news_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecycler.setLayoutManager(layoutManager);

        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextPage(page);
            }
        };

        mRecycler.addOnScrollListener(scrollListener);

        mApiClient.makeRequestAsync(Api.getApiInterface().getNews(1), new ResultListener<ApiResponse>() {
            @Override
            public void onRequestSuccess(ApiResponse response) throws Throwable {
                mNewsResponse = response.getResponse();

                if (response.getResponse().getStatus().equalsIgnoreCase("ok")) {
                    mAdapter = new NewsListAdapter(NewsActivity.this, response.getResponse().getArticles(), true);

                    mRecycler.setAdapter(mAdapter);
                }

                Toast.makeText(NewsActivity.this, "Success", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onRequestFailed(String errorMessage) {

                Toast.makeText(NewsActivity.this, "Failure", Toast.LENGTH_LONG).show();
            }
        });

//        loadFavorites();
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadFavorites();
    }

    private void loadFavorites() {
        mFavoritesRecycler = findViewById(R.id.news_favorite_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        mFavoritesRecycler.setLayoutManager(layoutManager);

        RealmResults<Article> favArticles = mRealm.where(Article.class)
                .equalTo("isFavorite", true)
                .sort("webPublicationDate", Sort.DESCENDING)
                .findAll();

        List<Article> articles = new ArrayList<>();
        articles.addAll(favArticles);
        if (articles.size() > 0) {
            NewsListAdapter favAdapter = new NewsListAdapter(this, articles, false);
            mFavoritesRecycler.setAdapter(favAdapter);
            mFavoritesRecycler.setVisibility(View.VISIBLE);
        }
        else {
            mFavoritesRecycler.setVisibility(View.GONE);
        }
    }

    private void loadNextPage(int page) {
        mApiClient.makeRequestAsync(Api.getApiInterface().getNews(page), new ResultListener<ApiResponse>() {
            @Override
            public void onRequestSuccess(ApiResponse response) throws Throwable {

                if (response.getResponse().getStatus().equalsIgnoreCase("ok")) {
                    if (mAdapter != null) {
                        mNewsResponse.getArticles().addAll(response.getResponse().getArticles());
                        mAdapter.notifyItemRangeInserted(mAdapter.getItemCount() - response.getResponse().getArticles().size(), mAdapter.getItemCount());
                    }
                    else {
                        mNewsResponse = response.getResponse();
                        mAdapter = new NewsListAdapter(NewsActivity.this, response.getResponse().getArticles(), true);

                        mRecycler.setAdapter(mAdapter);
                    }
                }

                Toast.makeText(NewsActivity.this, "Success", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onRequestFailed(String errorMessage) {

                Toast.makeText(NewsActivity.this, "Failure", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mRealm != null) {
            mRealm.close();
        }
    }
}
